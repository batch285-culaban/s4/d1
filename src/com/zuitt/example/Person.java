package com.zuitt.example;

public class Person implements Actions {

    public void sleep() {
        System.out.println("ZZZZZZZ...(snore)");
    }

    public void run(){
        System.out.println("Ruuuuning!!");
    }

    public void morningGreet(){
        System.out.println("Morning Sunshine!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays!");
    }
}
